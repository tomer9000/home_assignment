#!/bin/bash

filename=$(basename "$0")
function show_help() {
cat << EOF
Usage: ${filename} [OPTIONS]

This script inserts data into the MySQL database for company, account, and project.
It requires the following arguments for the data to be inserted.

Options:
  --company-id ID            The ID of the company (integer required).
  --company-name NAME        The name of the company (string required).
  --account-id ID            The ID of the account (integer required).
  --account-name NAME        The name of the account (string required).
  --project-id ID            The ID of the project (integer required).
  --project-name NAME        The name of the project (string required).
  --project-status STATUS    The status of the project (0 for Inactive, 1 for Active, 2 for Frozen).

Example:
${filename} --company-id 1 --company-name Panaya --account-id 4 --account-name VeryImportantCustomer --project-id 3 --project-name Maintenance --project-status 0

EOF
}

function validate_int() {
  if ! [[ "$1" =~ ^[0-9]+$ ]]; then
    echo "Error: $2 must be an integer." >&2
    exit 1
  fi
}

function validate_string() {
  if ! [[ "$1" =~ ^[a-zA-Z0-9._[:space:]-]+$ ]]; then
    echo "Error: $2 must be a string containing only alphanumeric characters, dots, underscores, hyphens, and spaces." >&2
    exit 1
  fi
}

function validate_status() {
  if ! [[ "$1" =~ ^[0-2]$ ]]; then
    echo "Error: --project-status must be 0, 1, or 2." >&2
    exit 1
  fi
}

if [[ "$#" -eq 0 ]] || [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
  show_help
  exit 0
fi

while [[ "$#" -gt 0 ]]; do
  case $1 in
    --company-id) validate_int "$2" "--company-id"; company_id="$2"; shift ;;
    --company-name) validate_string "$2" "--company-name"; company_name="$2"; shift ;;
    --account-id) validate_int "$2" "--account-id"; account_id="$2"; shift ;;
    --account-name) validate_string "$2" "--account-name"; account_name="$2"; shift ;;
    --project-id) validate_int "$2" "--project-id"; project_id="$2"; shift ;;
    --project-name) validate_string "$2" "--project-name"; project_name="$2"; shift ;;
    --project-status) validate_status "$2"; project_status="$2"; shift ;;
    *) echo "Unknown parameter passed: $1"; exit 1 ;;
  esac
  shift
done

sql_statements="INSERT INTO As_company (Id, Name) VALUES ($company_id, '$company_name');
INSERT INTO As_account (Id, Name, Company_id) VALUES ($account_id, '$account_name', $company_id);
INSERT INTO As_project (Id, Name, Account_id, Status) VALUES ($project_id, '$project_name', $account_id, $project_status);"

# Run SQL statements using MySQL client
mysql -u root -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE" -e "$sql_statements"



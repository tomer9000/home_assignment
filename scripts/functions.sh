function log() {
    echo "[$(date '+%Y-%m-%d_%H:%M:%S')] [INFO] ${1-}" | tee -a "${LOGFILE}"
}

function err() {
    echo "[$(date '+%Y-%m-%d_%H:%M:%S')] [ERROR] ${1-}" | tee -a "${LOGFILE}"
    exit 1
}

function build_run_mysql() {
    cd ../docker/panaya-mysql/
    log "Going to build panaya mysql image"
    docker build -t panaya-mysql-image . >> ${LOGFILE} 2>&1
    if [ $? -ne 0 ];then
        err "Could not build mysql image."
    fi
    log "mysql image was built successfully."

    log "Creating infrastracture for mysql - network and volume"
    docker volume create mysql_data >> ${LOGFILE} 2>&1
    if [ $? -ne 0 ];then
        err "Could not create mysql_data volume."
    fi
    docker network create mysql_network >> ${LOGFILE} 2>&1
    if [ $? -ne 0 ];then
        err "Could not create mysql_network network."
    fi

    log "Going to run mysql container"
    docker run -d -p 3306:3306 --name panaya-mysql-container -v mysql_data:/var/lib/mysql --network mysql_network panaya-mysql-image >> ${LOGFILE} 2>&1
    if [ $? -ne 0 ];then
        err "Could not run mysql container."
    fi
    log "mysql container is successfully running."
}


function build_run_webserver() {
    cd ../docker/panaya-webserver/
    log "Going to build panaya webserver image"
    docker build --network=mysql_network -t panaya-webserver-image . >> ${LOGFILE} 2>&1
    if [ $? -ne 0 ];then
        err "Could not build webserver image."
    fi
    log "webserver image was built successfully."

    log "Going to run webserver container"
    docker run -p 9980:9980 -d --name panaya-webserver-container panaya-webserver-image >> ${LOGFILE} 2>&1
    if [ $? -ne 0 ];then
        err "Could not run webserver container."
    fi
    log "webserver container is successfully running."
}

function remove_webserver() {
    log "Going to remove webserver infrastructure"
    if docker ps -a | grep -q panaya-webserver-container; then
        log "Going to remove webserver container"
        docker rm --force panaya-webserver-container >> ${LOGFILE} 2>&1
        if [ $? -ne 0 ];then
            err "Could not remove mysql container."
        fi
    fi
    if docker images | grep -q panaya-webserver-image; then
        log "Going to remove webserver image"
        docker rmi panaya-webserver-image >> ${LOGFILE} 2>&1
        if [ $? -ne 0 ];then
            err "Could not remove webserver image."
        fi
    fi
    log "Going to remove webserver stage 1 builder image"
    docker images --filter "label=builder=true" --quiet | xargs docker rmi  >> ${LOGFILE} 2>&1
}

function remove_mysql() {
    log "Going to remove panaya mysql infrastructure"
    if docker ps -a | grep -q panaya-mysql-container; then
        log "Going to remove mysql container"
        docker rm --force panaya-mysql-container >> ${LOGFILE} 2>&1
        if [ $? -ne 0 ];then
            err "Could not remove mysql container."
        fi
    fi
    if docker images | grep -q panaya-mysql-image; then
        log "Going to remove mysql image"
        docker rmi panaya-mysql-image >> ${LOGFILE} 2>&1
        if [ $? -ne 0 ];then
            err "Could not remove mysql image."
        fi
    fi
    if docker network ls | grep -q mysql_network; then
        log "Going to remove mysql network"
        docker network rm mysql_network >> ${LOGFILE} 2>&1
        if [ $? -ne 0 ];then
            err "Could not remove mysql network."
        fi
    fi
    if docker volume ls | grep -q mysql_data; then
        log "Going to remove mysql data"
        docker volume rm mysql_data >> ${LOGFILE} 2>&1
        if [ $? -ne 0 ];then
            err "Could not remove mysql network."
        fi
    fi
    log "Successfully removed mysql infrastructure."
}
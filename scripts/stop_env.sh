#!/bin/bash

basepath=`dirname $(readlink -f $0)`
cd ${basepath}
export LOGFILE_relative_path="../logs/stop_env.log"
echo "" > ${LOGFILE_relative_path}
export LOGFILE=$(realpath ${LOGFILE_relative_path})
source functions.sh

log "Going to stop the environment"
remove_webserver
remove_mysql
